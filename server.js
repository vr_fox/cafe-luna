const http = require('http');
const env = require('dotenv').config();
const port = process.env.PORT || 3000;
const app = require('./server/app');

const server = http.createServer(app);

server.on('listening', function(){
     console.log('Listening to ', port);
});
  
server.on('error', function(err){
    console.log(err);
});

server.listen(port);

